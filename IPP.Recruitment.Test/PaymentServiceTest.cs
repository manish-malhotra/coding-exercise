﻿using IPP.Recruitment.Payments;
using IPP.Recruitment.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace IPP.Recruitment.Test
{
    class PaymentServiceTest
    {
        private Mock<IPaymentService> _service;

        #region ValidCheck

        [TestMethod]
        public void ValidWhatsYourId()
        {
            _service = new Mock<IPaymentService>();
            _service.Setup(o => o.WhatsYourId()).Returns(Constants.WhatsYourId);  // Checking Valid Id from constant class

            var tests = new Payment(_service.Object);

            var result = tests.WhatsYourId();
            Assert.AreEqual(result, "008ab27c-36b2-43e5-91d5-edbd1e5b564b");
        }


        [TestMethod]
        public void ValidCreditCard()
        {
            var creditCard = new CreditCard()
            {
                Amount = 150,
                ExpiryMonth = 10,
                ExpiryYear = 2018,
                CardNumber = "1234567899876543"
            };

            var tests = new Payment(new PaymentService());
            var result = tests.MakePayment(creditCard);
            Assert.AreNotEqual(result, string.Empty);

        }

        #endregion

        #region InvalidCheck

        [TestMethod]
        public void InvalidMonth()
        {
            var creditCard = new CreditCard()
            {
                Amount = 2200,
                ExpiryMonth = 21, // Invalid Month
                ExpiryYear = 2019,
                CardNumber = "1234567899876543"
            };

            var tests = new Payment(new PaymentService());
            var result = tests.MakePayment(creditCard);
            Assert.AreEqual(result, string.Empty);

        }

        [TestMethod]
        public void InvalidYear()
        {
            var creditCard = new CreditCard()
            {
                Amount = 2200,
                ExpiryMonth = 11,
                ExpiryYear = 2011, // Invalid Year
                CardNumber = "1234567899876543"
            };

            var tests = new Payment(new PaymentService());
            var result = tests.MakePayment(creditCard);
            Assert.AreEqual(result, string.Empty);

        }

        [TestMethod]
        public void InvalidMinAmount()
        {
            var creditCard = new CreditCard()
            {
                Amount = 4, // Invalid Minimum Amount
                ExpiryMonth = 11,
                ExpiryYear = 2019,
                CardNumber = "1234567899876543"
            };

            var tests = new Payment(new PaymentService());
            var result = tests.MakePayment(creditCard);
            Assert.AreEqual(result, string.Empty);

        }

        [TestMethod]
        public void InvalidMaxAmount()
        {
            var creditCard = new CreditCard()
            {
                Amount = 99999999999,  // Invalid Maximum Amount
                ExpiryMonth = 11,
                ExpiryYear = 2019,
                CardNumber = "1234567899876543"
            };

            var tests = new Payment(new PaymentService());
            var result = tests.MakePayment(creditCard);
            Assert.AreEqual(result, string.Empty);

        }

        [TestMethod]
        public void InvalidCreditCardNumber()
        {
            var id = Guid.NewGuid();

            var creditCard = new CreditCard()
            {
                Amount = 10000,
                ExpiryMonth = 10,
                ExpiryYear = 2019,
                CardNumber = "xyzabc7899876543"  // Invalid Credit Card Number
            };

            var tests = new Payment(new PaymentService());
            var result = tests.MakePayment(creditCard);
            Assert.AreEqual(result, String.Empty);

        }

        #endregion
    }
}
