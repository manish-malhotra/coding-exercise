﻿using System.Linq;
namespace IPP.Recruitment.Domain.BusinessRules
{
    /// <summary>
    /// Performs a Mod-10/LUHN check on the passed number and returns true if the check passed
    /// </summary>
    /// <param name="cardNumber">A 16 digit card number</param>
    /// <returns>true if the card number is valid, otherwise false</returns>
    /// <remarks>
    /// Refer here for MOD10 algorithm: https://en.wikipedia.org/wiki/Luhn_algorithm
    /// </remarks>
    public class IsCardNumberValid : IBusinessRule
    {
        private string _creditCardNumber;
        public IsCardNumberValid(string cardNumber)
        {
            _creditCardNumber = cardNumber;
        }
        public bool Validate()
        {
            return _creditCardNumber.All(char.IsDigit) && _creditCardNumber.Reverse()
             .Select(c => c - 48)
             .Select((thisNum, i) => i % 2 == 0
                 ? thisNum
                 : ((thisNum *= 2) > 9 ? thisNum - 9 : thisNum)
             ).Sum() % 10 == 0;
        }
    }
}
