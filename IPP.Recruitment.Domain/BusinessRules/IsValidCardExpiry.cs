﻿using System;

namespace IPP.Recruitment.Domain.BusinessRules
{
    public class IsValidCardExpiry: IBusinessRule
    {
        private int _validExpiryMonth;
        private int _validExpiryYear;
        public IsValidCardExpiry(int expiryMonth, int expiryYear)
        {
            _validExpiryMonth = expiryMonth;
            _validExpiryYear = expiryYear;

        }

        public bool Validate()
        {
            return !((_validExpiryMonth < Constants.minMonth || _validExpiryMonth > Constants.maxMonth)
                || (_validExpiryYear < DateTime.Today.Year || _validExpiryYear > Constants.maxYear)
               || (_validExpiryYear == DateTime.Today.Year && _validExpiryMonth <= DateTime.Today.Month)
           );
        }

    }
}
