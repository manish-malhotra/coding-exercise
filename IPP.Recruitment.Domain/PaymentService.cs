﻿using System;
using System.Collections.Generic;
using IPP.Recruitment.Domain.BusinessRules;

namespace IPP.Recruitment.Domain
{
    public class PaymentService : IPaymentService
    {
        public string WhatsYourId()
        {
            return Constants.WhatsYourId;
        }
    
        public Guid? MakePayment(CreditCard cardDetails)
        {
            var validRules = new List<IBusinessRule>();
            // validRules.Add(new IsCardNumberValid(cardDetails.CardNumber)); Added this rule in CanMakePaymentWithCard...
            validRules.Add(new IsValidPaymentAmount(cardDetails.Amount));
            validRules.Add(new CanMakePaymentWithCard(cardDetails.CardNumber, cardDetails.ExpiryMonth, cardDetails.ExpiryYear));
            if (validRules.TrueForAll(x => x.Validate()))
                return Guid.NewGuid();
            else
                return Guid.Empty;
        }
    }
}
