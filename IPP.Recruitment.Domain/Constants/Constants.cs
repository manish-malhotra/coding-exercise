﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPP.Recruitment.Domain
{
    public class Constants
    {
        public const int minMonth = 1;
        public const int maxMonth = 12;
        public const int maxYear = 2050;
        public const long minAmount = 99;
        public const long MaxAmount = 99999999;
        public const string WhatsYourId = "008ab27c-36b2-43e5-91d5-edbd1e5b564b";
        public const string invalidCreditCard = "Invalid Credit Card";
    }
}
