﻿using System;
using IPP.Recruitment.Domain;

namespace IPP.Recruitment.Payments
{
    public class Payment
    {
        private IPaymentService _service;

        public Payment(IPaymentService service)
        {
            _service = service;
        }

        public string MakePayment(CreditCard cardDetails)
        {
            var response = _service.MakePayment(cardDetails).ToString();
            if (response == Guid.Empty.ToString())
                return Constants.invalidCreditCard;
            else
                return response;
        }

        public string WhatsYourId()
        {
            return _service.WhatsYourId();
        }
    }
}
