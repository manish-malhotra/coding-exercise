﻿using System.Web.Http;
using IPP.Recruitment.Payments;
using IPP.Recruitment.Domain;
using System;

namespace CreditCardService.Controllers
{
    public class PaymentServiceController : ApiController
    {
        private IPaymentService _service;
        public PaymentServiceController(IPaymentService service)
        {
            _service = service;
        }
        public PaymentServiceController()
        {
            _service = new PaymentService(); 
        }
        //Get
        [HttpGet]
        public IHttpActionResult WhatsYourId()
        {
            var payments = new Payment(_service);

            try
            {
                var result = payments.WhatsYourId();
                return Ok(result);
            }
            catch
            {
                return InternalServerError();
            }
        }
        //Post
        [HttpPost]
        public IHttpActionResult MakePayment([FromBody]CreditCard cardData)
        {
            var payments = new Payment(_service);
            try
            {
                var result = payments.MakePayment(cardData);
                if (result != string.Empty)
                    return Ok(result);
                else
                    return BadRequest("Invalid Credit Card");
            }
            catch(Exception ex)
            {
                return InternalServerError();
            }
        }
    }
}